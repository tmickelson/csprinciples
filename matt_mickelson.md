# AI-Enabled Cyber

- Language translation in pictures is very hard becuase of the nessicary steps. 
- AI is changing the way that technology is abused. 
  - Voice assistance
  - Google maps fake traffic
- Neural Networks
  - Developed to mimic human thinking
  - Every node can be connected to any other node in an adjacent layer
  - These networks can be any size
- Convolutions
  - Input layer looks at many different parts of the image
  - Lower image quality, makes processing much faster
- Recursion
  - Reusing a step on itself
  - Use some short term memory
- Sequence matching
  - Used in translation
- AI is probablistic, it will not always give the same response. 
- One the "bad guys" use AI there is not enought time for a human response to the attack
- Edge cases can cause many issues
- Dogs are cute (slide 24)
- Only create an algotithm that is validatible

