# Big Idea 4: Computing Systems and Networks 


## Main ideas

- Built-in **redundancy** enables the Internet to continue working when parts
  of it are not operational.
- Communication on the Internet is defined by **protocol** such as
  TCP/IP that determine how messages to send and receive data are formatted.
- Data sent through the Internet is broken into **packets** of equal size.
- The Internet is **scalable**, which means that new capacity can be quickly
  added to it as demand grows.
- The **World Wide Web** (WWW) is a system that uses the Internet to share
  web pages and data of all types.
- Computing systems can be in **sequential**, **parallel**, and **distributed**
  configurations as they process data.


## Vocabulary

- Bandwidth
  - The speed at which data can be sent at
- Computing device
  - A device that can run arithmetical or logical operations
- Computing network
  - A network of computing devices that share data and/or processing power
- Computing system
  - A group of computing devices that work together for a common purpose
- Data stream
  - The transmission of data between two or more computing devices
- Distributed computing system
  - Computing systems that are split over a network
- Fault-tolerant
  - The ability for a computer to continue working after a component fails
- Hypertext Transfer Protocol (HTTP)
  - The protocol that many webpages used to use
- Hypertext Transfer Protocol Secure (HTTPS)
  - The secure version of HTTP, almost all webpages use it
- Internet Protocol (IP) address
  - An address that identifies any device connected to a network
- Packets
  - One section of the data being sent
- Parallel computing system
  - A type of computation where many calculations are carried out simultaneously
- Protocols
  - A set of rules for fromatting and processing data
- Redundancy
  - Extra components or data that is used as a backup
- Router
  - A device that forwards packets between devices and other networks
