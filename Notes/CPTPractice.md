# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
> The purpose of this project is to make a small game where you run away from robots. 

2. Describes what functionality of the program is demonstrated in the
   video.
> Each time you move, the robots move towards the player. If two robots collide they become junk. If the player runs into either a living or a dead robots the game ends.

3. Describes the input and output of the program demonstrated in the
   video.
> Input: A key is pressed
>
> Output: The player moves, then the robot moves towards the player

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
> ![Code Segment #1](./codeSegment1.png)

> This code creates a list called "robots", and filled it with the robot objects. The robots list is declared on line 35, and fill on lines 37 through 43.

2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
> ![Code Segment #2](./codeSegment2.png)

> This code accesses the each robot object and moves it towards the player. It selects an object on line 91, and moves the robot towards the player on lines 92 to 103.


## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
> The name of the list that is used in this response in called "robots". It is declared on line 35.

2. Describes what the data contained in the list represent in your
   program
> The data contained in the list repersents all of the living robots.

3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
> If the list was not included in the code, it would be much harder to change the number of robots. 

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
> ![Code Segment #3](./codeSegment3.png)

> This procedure returns either true or false. 
> It takes two inputs, an item and list, (line 106) and checks if the item is in the same spot as anything in the list (line 108). 
> It loops through the list to check if the item is in the same spot as anything in the list (line 107).

2. The second program code segment must show where your student-developed
   procedure is being called in your program.
> ![Code Segment #4](./codeSegment4.png)

> The procedure is used to check if the player is in the same spot as any robots. The check is run on line 126.

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
> The procedure checks if an item is in the same spot as anything in the list.
> It ends the game if the player is caught and kills robots that are in the same spot as junk. The check happens on line 108.

4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
> The procedure loops through the list. While looping, it checks if the x and y position of the item are the same as the x and y position of the current item from the list. 

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
> First call (line 126):
> The first call passes the arguments "player" for the item and "robots" for the list.
> ![Code Segment #4](./codeSegment4.png)

> Second call (line 136):
> The second call passes the arguments "robot" for the item and "junk" for the list
> ![Code Segment #5](./codeSegment5.png)

2. Describes what condition(s) is being tested by each call to the procedure
> ![Code Segment #3](./codeSegment3.png)

> Condition(s) tested by the first call (line 108):
> The first call tests if the player is in the same spot as any of the robots. 

> Condition(s) tested by the second call (line 108):
> The second call tests if a robot is in the same spot as any other dead robots. 

3. Identifies the result of each call
> ![Code Segment #3](./codeSegment3.png)

> Result of the first call (line 109 or 110):
> If the player is in the same spot as any of the robots, then it returns true. Other wise it returns false

> Result of the second call (line 109 or 110):
> If the selected robot is in the same spot as any of the junk, then it returns true. Other wise it returns false

