# Intro to Networking

## Chapter 2

### Overview

There are four main areas in network engineering:
- Application
- Transport
- Internetwork
- Link

This model is called the TCP/IP (Transport Control Protocol/Internet Protocol).

### Layers

Link:
The link layer connects your device with the rest of the local network. There are many connections types. WiFi, cellular, and Ethernet are all examples of a connection type. The link layer needs to solve two problems: data encoding method and what frequency. It also needs to get computers to send their data in an orderly manner. Having the computers break their messages into packets solves two of these three problems. 
To make sure that two computers don't send data at the same time, they use CSMA/CD (Carrier Sense Multiple Access with Collision Detection). Before a device sends data, it checks to see if the receiving device is sending data. While the computer is sending data, it listens to see if receives its data. If it receives its data, it knows that the channel is still clear. If it did not receive its data, it knows that the channel is also being used and packets collided. It then waits a bit before trying again. 
To send data far distances, multiple link layers are chained together. 

Internetwork:
Each packet has a source address, and a destination address. Each router does not know the exact location for the destination, so it guesses, sending the packet closer to the destination. The closer to the destination the packet gets, the better of a guess each router has on where the packet needs to go. Because of the nature of a network, if a router is down, packets can easily be rerouted. 

Transport:
Sometimes packets can get lost or delayed, so sometimes they come in out of order. To correct this, each packet has a number. If a packet never arrives, the device asks for the specific packet again. 
The window size is how much data a computer sends before waiting for acknowledgment. 

Application:
The application layer is where the computer interacts with the rest of the network. Some examples of the application layer include Internet Explorer, Thunderbird, and Google Drive.
This layer is split into two parts, the server and the client. The server waits for an incoming packet asking for something. It then send data back to the client.

## Chapter 3

Whenever a computer has WiFi enabled, it can hear all packets that are sent to and from all nearby routers. To help make sure that packets go to the correct router, each time your computer connects, it asks for the router's MAC address. The router then responds saying that it is the router. 

If there are many WiFi radios transmitting data at the same time (or you have the microwave on), the signals will collide. To prevent this, devices use Carrier Sense. When a device wants to transmit, it first checks to see if any other packets are being sent at that time. If there are none, then it sends data. 

Link layers communicate with each other without speaking over others by passing a token around. Similar to how some elementary school classes have a talking stick. The token method is best for long transmission times, while CSMA/CD works best with short transmission times.

## Chapter 4

The internetwork layer is how a packet goes from the source router to the destination router. This layer can be split over many different mediums (ie: satellite or fiber optics). Each stop the packet makes, the closer it gets to the more accurate the guess is. 

The IP address was created to act as a modernized version of the MAC address because not every router can know every device. The first two parts of an IPv4 address (XXX.XXX.\*.\*) is the network number. The second half (\*.\*.XXX.XXX) is the device identifier. 

A new router has no idea where to send packets. As it sends packets, it learns which routers are where. If a packet fails to send, it sends the packet to its neighbors. Each packet has a time to live (TTL). The TTL decreases with each connection it passes through. Once the TTL is zero, the packet is discarded. This prevents infinite loops:
```
    A -x B
  /  \
 D -- C
```
(A fails to send packet to B, so it sends it to C. C does not know B so it send it to D. The same with D, so it directs it back to A)

Each time a computer connects to a network, it is assigned an IP address by a DHCP. Local IP addresses, only known/accessible to those inside the network, usually are 192.168.\*.\*. Global IP addresses are assigned by ISPs (Internet Service Providers) whenever you connect to the internet.

## Chapter 5

DNS (Domain Name Servers) allows for the use of domain names instead of IP addresses. TDLs (Top-Level Domains) are managed by large organizations. ICANN (International Committee for Assigned Names and Numbers) assigns the TDLs to the corporations. ccTDLs (Country Code Top-Level Domains) are two letter domain names assigned to countries. Subdomains (THIS-PART.domain.net) can be created once a domain is owned. Unlike IP addresses, where the most specific part is on the right, domain name have the least specific part on the right, gaining specificity as you move left.

## Chapter 6

Each packet has a header consisting of the router it is from and going to (changes between routers); who it is from, to, and the TTL (decreases between routers); and the port and offset. The sending device must keep a copy of the packets, in case the receiving device requests that a packet be sent again. 

### Parts of a packet:

- Link Header
  - To: the router the packet is headed to
  - From: the router the packet is headed from
- IP Header
  - To: who the packet is to
  - From: who the packet is to
  - TTL: time to live for the packet
- TCP Header
  - Port: the port the packet is sent on
  - Offset: the number to reorder the packets

Ports allow you to use one URL for many uses. For example `mail.company.com` when accessed from a web browser, get web mail, but when used in a mail application, fetches and sends mail. 

Common Ports:
| Name   | Port Number | Use                    |
| ------ | ----------- | ---------------------- |
| Telnet | 23          | Login                  |
| SSH    | 22          | Secure Login           |
| HTTP   | 80          | World Wide Web         |
| HTTPS  | 443         | Secure Web             |
| SMTP   | 25          | Incoming Mail          |
| IMAP   | 143/220/993 | Mail Retrieval         |
| POP    | 109/110     | Older Mail Retrieval   |
| DNS    | 53          | Domain Name Resolution |
| FTP    | 21          | File Transfer          |

## Chapter 7

There are two types of applications, server and client. The server application runs on the server and responds to requests. The client application runs one the client (your device) and sends requests for data or sends data. 

A protocol is a set of rules that describe how an action should be preformed. There are many different protocols. 

The first application designed for HTTP was telnet. It was developed in 1968, before the first TCP/IP network was created. Telnet requires one argument, but can easily be used with two. The first one is the address for the server you are trying to connect to. The second one is the port. 

If the protocol is not followed the server ends the connection

Another common protocol is IMAP (Internet Message Access Protocol). It looks more like a conversation between humans that HTTP does because it works by sending many RFC (Request for Comment) documents.

Sometimes images load from the top down because the packets for the image come in slowly.

Many programming languages have buildin libraries that handle opening connections to and hosing servers. 

## Chapter 8

When the internet just started out, there was no need for protecting the data. They were so small that they could easily be protected. In the 1990s secuirity became more of a problem because everything was beign transmitted wirelessly There were two solutions: keep that data transmission safe, or encrypt the data. The latter option was chosen for its simplicity compaired to the former. 

Enctyption has existed for a long time. It started out with the Ceaser Cipher, shifting each letter a fixxed amount. Later on shared keys were invented, adding much more protection, but giving out the key was hard. The asymetric key fixes the the problems that the shared key has. You give out the public key for anyone to encrypt items with, then you use the peivate key to decrypt the data. 

SSL (Secure Sockets Layer) aka TLS (Transport Layer Security) is a layer that was added to the TCP/IP stack as to not mess with the preexisting protocols. 

Web browsers use HTTPS for encrypted traffic and HTTP for unencrypted traffic. HTTPS is slightly more resource intensive than HTTP because it has to encrypt and decrypt the date. 

Keys are sent to a CA (Certificate Authority) for verification. If a key is not signed by a CA, it prompts a warning about connection secuirity. 
