# Big Idea 5: Impact of Computing 


## Main ideas

- New technologies can have both beneficial and unintended harmful effects,
  including the presence of bias.
- Citizen scientists are able to participate in identification and problem
  solving.
- The digital divide keeps some people from participating in global and local
  issues and events.
- Licensing of people’s work and providing attribution are essential.
- We need to be aware of and protect information about ourselves online.
- Public key encryption and multifactor authentication are used to protect
  sensitive information.
- Malware is software designed to damage your files or capture your sensitive
  data, such as passwords or confidential information.


## Vocabulary

- asymmetric ciphers
  - Public and private key encryption. Involve large prime numbers. The public key encrypts and the private key decrypts.
- authentication
  - Proving that you are who you say you are. 
- bias
  - Descriminating agains groups or individuals while favoring others. 
- Certificate Authority (CA)
  - A Certificate Authority is a corporation that issues SSL certificates. SSL certificates are used for encrypting HTTPS traffic. 
- citizen science
  - Citizen science is research conducted by non-professional scientists.
- Creative Commons licensing
  - Creative Commons licensing allows work to be copyrighted while still allowing certain groups of people (usually non-commercial) to use, share, and build upon it.
- crowdsourcing
  - Crowdsourcing is when many volunteers work on project. One example of this is Wikipedia.
- cybersecurity
  - Cybersecurity is the protection of computer networks and systems along with the data stored on them.
- data mining
  - Data mining is a process that extracts patterns of vast data sets.
- decryption
  - Decrypting is the process of un-encrypting encrypted data.
- digital divide
  - The digital divide is inequality in what technological devices and networks groups and people have.
- encryption
  - Encrypting data is a method of encoding data where only the sender and reciver should be able to read the plaintext.
- intellectual property (IP)
  - Intellectual property is and tangible and intangible creations. This includes: copyrights, trademarks, and patents.
- keylogging
  - Keyloggers record keypresses as they are typed. 
- malware
  - Software that was created steal data or damage files.
- multifactor authentication
  - Multifactor authentication only allows users to log in once they present two means of authentication. 
- open access
  - Open access is a set of principals that allows for any type of digital content to be access free of charge.
- open source
  - Source code that has been released for the public to read, modify, and redistribute.
- free software
  - Open source software.
- FOSS
  - Software that is both free and open source. Stands for Free and open-source software.
- PII (personally identifiable information)
  - Data that is related to an identifiable person.
- phishing
  - Tricking people into giving out private information.
- plagiarism
  - Passing someone else's work off as your own.
- public key encryption
  - Another name for asymmetric cypher.
- rogue access point
  - A wireless access point that has been installed without permission
- targeted marketing
  - Advertizing that is directed towards a certain group of people. 
- virus
  - A self-replicating, self-spreading piece of code.
