# What does our project do

We coded a small game where you are chased by an enemy.

# How does it work

First it initializes the player and the enemy:
```Python
player_position = 2 
enemy_position = 7 
enemy_direction = random.randint(1, 2)
player_has_moved = False
```

Then the game starts. It consists of endless rounds where the player moves away from the player until it is caught. 

The round starts by checking if the has collided with the enemy:
```Python
if player_position == enemy_position:
    for a in range(10):
        cp.pixels[a] = (255, 0, 0)
    break
```

Then it checks which way to move the player:
```Python
if cp.button_a:
    if player_position != 0:
        player_position -= 1
    else:
        player_position = 9 
    player_has_moved = True
else if cp.button_b:
    if player_position != 9:
        player_position += 1
    else:
            player_position = 0 
        player_has_moved = True
```

After the player moves, the enemy moves:
```Python
if player_has_moved:
    if random.randint(1, 10) == 1:
        if enemy_direction == 2:
            enemy_direction = 1
        else:
            enemy_direction = 2
    if enemy_direction == 2:
        if enemy_position != 0:
            enemy_position -= 1
        else:
            enemy_position = 9
    else:
        if enemy_position != 9:
            enemy_position += 1
        else:
            enemy_position = 0
    player_has_moved = False
```
