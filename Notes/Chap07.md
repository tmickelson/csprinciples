# Chapter 7 Notes

## Definitions

- `for`: repeats code (with variable that tells you what loop you're at)
- `range`: creates a list of numbers

## For loop

`for <variable (commonly 'i')> in <list_of_numbers (range(n))>:`  
`    [code to execute]`  
`    [more code to execute]`  
  
The variable is set to the value of the number in the list of numbers. It is usally the number of the current loop. (Index starting at 0 with range())  

## Lists

`fibonacci_numbers = [1, 1, 2, 3, 5]`  
`animals = ["cat", "dog", "fist", "bird"]`  
  
In python, a list (or an array) is a set of variables enclosed by [ and ] and seperated by commas (',').  

## Range

`range(10)` returns:  
`[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]`  
  
`range(20, 31)` returns:  
`[20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]`  
  
The range function with one argument (range(n)) returns a list of intergers from 0 to n. The range function with two arguments (range(n, m)) returns a list of intergers from n to m - 1.  
  
`range(0, 20, 2)` returns:  
`[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]`  
  
`range(9, -1, -1)` returns:  
`[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]`  

When provided with a third argument(range(n, m, l)), the range function steps. Starting at n, increasing by l, to (m - 1). The range function can also count down (range(\<start\>, \<end - 1\>, -\<step\>)).  

