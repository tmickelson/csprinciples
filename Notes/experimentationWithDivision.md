# Experimentation With Division

When you divide two integers, you are using interger divission(/). In python 3, dividing two intergers can return a double, but in many other programs it returns rounded down. 

If you want to round the answer down in python 3 you can use floor division (//). 

It is very simple to get the remainder of two numbers. All you have to do is use the modulo opperator (%).

For examples, please see [this directory](https://codeberg.org/tmickelson/csprinciples/src/branch/main/Programs/divisionExamples).
