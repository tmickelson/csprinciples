# Chapter 3: Notes for Numbers Notes

## Assigning a Name

A computer stores values in variables. Variables can change \(vary\). A variable's value is sometimes calles an assignment. 

A variable must start with a letter \(A or a\) or an underscore \(\_\). After that it can contain numbers \(0-9\). Variable names cannot be python keywords such as:
- else
- if
- for
- or
- not
- return
- while

Variable names are case-sensitive

Becasue you can\'t have spaces in variable names, there are two systems; camelCase and mixed\_case.


## Expressions

The right side of an assignment can be arithmetic. For example, myVar = 1 + 2. To divide numbers use \'\/\'. Note that python3 converts ints into doubles for division \(Will return double\). If you want to divide ints as ints use \"\/\/\" \(Will return int rounded down\). 

Variables can also be set to other variables. For example, myVar = yourVar.

Modulo \(\%\) gets the remainder of two number \(Just like mod\(\) function\). For example, 5 \% 2 will return 1.


## Expressions Summary

| Expression | Meaning        |
| ---------- | -------------- |
| x + y      | Addition       |
| x \- y     | Subtraction    |
| x * y      | Multiplication |
| x \/ y     | Division       |
| x \/\/ y   | Floor Division |
| x \% y     | Modulo         |
| -x         | Negation       |

Expressions are evaluated the same as in math class \(PEMDAS or GEMDAS\). You can make more complex expressions like:<br />
`var1 = x + y * z`<br />
`var2 = x / var1`<br />
`var3 = var1 % var2`<br />

