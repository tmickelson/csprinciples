# AP CSP

## AP CSP Scoring

I could not find any information on the scoring of the AP CSP exam.

## Pre-Test

I did the best on Big Idea #4: Copmuting Systems and Networks. I did the worst on Big Idea #5: Impacts of computing

## Studing Resources

Some resources that I found that will help me study are:

- [Exam Description](https://apcentral.collegeboard.org/media/pdf/ap-computer-science-principles-course-and-exam-description.pdf)
- [About the Exam](https://apstudents.collegeboard.org/courses/ap-computer-science-principles/assessment)
- [Khan Academy](https://www.khanacademy.org/computing/ap-computer-science-principles/ap-csp-exam-preparation/prepare-for-the-2019-ap-cs-p-exam/a/ap-cs-p-exam-format)
