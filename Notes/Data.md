# Big Idea 2: Data 


## Main ideas

- Abstractions such as numbers, colors, text, and instructions can be
  represented by binary data ("It's all just bits!").
- Numbers can be converted from one number system to another. Computers use
  the binary number system ("It's all just bits!" ;-).
- **Metadata** helps us find and organize data.
- Computers can process and organize data much faster and more accurately than
  people can.
- [Data mining](https://en.wikipedia.org/wiki/Data_mining), the process of
  using computer programs to sift through data to look for patterns and trends,
  can lead to new insights and knowledge.
- It is important to be aware of bias in the data collection process.
- Communicating information visually helps get the message across.
- **Scalability** is key to processing large datasets effectively and
  efficiently.
- Increasing needs for storage led to the development of data compression
  techniques, both lossless and lossy.


## Vocabulary

- abstraction
  - a method of storing a complex idea as bits (ie: text as ASCII or numbers as binary)
- analog data
  - data that is stored any number of values (ie: a song stored on a record, but not a CD)
- bias
  - data that does not accurately represent its source
- binary number system
  - a number system that only uses two numbers, 1 and 0
- bit
  - a singular 1 or 0
- byte
  - a group of eight 1s or 0s (can store 256 unique values)
- classifying data
  - analyzing and structuring unstructured data
- cleaning data
  - removing incorrect, incomplete, and broken data (corrupted/incorrectly formatted) from a dataset 
- digital data
  - data that is stored in binary (ie: numbers or text)
- filtering data
  - choosing smaller parts of data for analysis or viewing
- information
  - the abstract concept of data, can be any form (ie: paintings, spoken words)
- lossless data compression
  - a type of data compression that produces an exact copy of the original data
- lossy data compression
  - a type of data compression that causes irreversible data loss
- metadata
  - data that provides information on the data (ie: owner or file perms.)
- overflow error
  - a number takes up more space than provided (it wraps back around to 0 the negative version of the largest number)
- patterns in data
  - parts of data that repeat
- round-off or rounding error
  - data lost while rounding
- scalability
  - the property of a system being able to handle more data via expansion of the hardware
