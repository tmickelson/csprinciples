# Big Idea 1: Creative Development

## Main ideas

- Collaboration on projects with diverse team members representing a variety
  of backgrounds, experiences, and perspectives can develop a better computing
  product.
- A *development process* should be used to design, code, and implement
  software that includes using feedback, testing, and reflection.
- Software documentation should include the programs requirements, constraints,
  and purpose.
- Software should be adequately tested before it is released.
- *Debugging* is the process of finding and correcting errors in software.


## Vocabulary

- code segment
  - A small segment of code
- collaboration
  - Working together to achieve a common goal
- comments
  - Seccions of code that the compilier does not read/interpret
- debugging
  - The process of finding bug (errors) in code
- event-driven programming
  - Programming that is made to handle user input (mouse, keyboard, ect.)
- incremental development process
  - A development process that builds upon the previous code
- iterative development process
  - A development process that splits large programs into smaller parts
- logic error
  - When the program runs without problem, but does not produce the expected output
- overflow error
  - When a number exceeds the maximum value allowed, the number then rolls over
- program
  - A list of insturction that a computer executes
- program behavior
  - How the program behaves when run
- program input
  - The input that a program recieves when run and while running (mouse, keyboard, text, ect)
- program output
  - How the program communicates with other parts of the computer (print text, display image, edit files, ect.)
- prototype
  - An early model or design made to test a concept
- requirements
  - Items that are needed before something can happen
- runtime error
  - An error that occurs while running the program (not while compiling)
- syntax error
  - An error that occurs because the code was not formatted properly and the compiler could not parse it
- testing
  - Verifing that the program runs as expected
- user interface
  - How a user interacts with the computer (GUI, CLI, ect.)


## Computing Innovation

A *computer artifact* is anything created using a computer, including apps,
games, images, videos, audio files, 3D-printed objects, and websites.
*Computing innovations* are innovations which include a computer program as a
core part of its functionality.  GPS and digital maps are examples of
computing innovations that build upon the early non-computer innnovation of
map making.


## Development Process 

![Software Development Life Cycle](resources/SDLF.svg)


## Errors

Four types of programming errors need to be understood:

1. Syntax errors
2. Runtime errors
3. Logic errors
4. Overflow errors

Which type of error is this?
```python
for i in range(10)
    print(i)
```
Syntax error

Which type of error is this?
```python
# Add the numbers from 1 to 10 and print the result
total = 0
for num in range(10)
    total += num 
print(total)
```
Logical and syntax error

Which type of error is this?
```python
nums = [3, 5, 8, 0, 9, 11]
result = 1
for num in nums:
    result = result / num
print(result)
```
Runtime error

