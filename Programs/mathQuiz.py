from random import randint

correct = 0
numOfQuestions = int(input("How many questions >>> "))

for i in range(numOfQuestions):
    num1 = randint(1, 10)
    num2 = randint(1, 10)
    question = f"\nWhat is {num1} times {num2}? >>> "
    response = int(input(question))
    answer = num1 * num2
    if response == answer:
        print("That's right - well done.")
        correct += 1
    else:
        print(f"No, I'm afrad the answer is {answer}")

print(f"\nI asked you {numOfQuestions} questions. You got {correct} of them right.")

if correct == 0:
    print("You have failed, maybe next time accualy put in some effort.")
elif correct / numOfQuestions == 1:
    print("Excelent Job!")
elif correct / numOfQuestions >= 0.8:
    print("Good Job!")
elif correct / numOfQuestions >= 0.5:
    print("You need to study a bit more.")
elif correct / numOfQuestions > 0:
    print("You need to study a lot more.")
else:
    print("You got more questions correct than I asked you A+")
