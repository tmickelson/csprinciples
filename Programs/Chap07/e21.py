def math_thing(end):
    """
      >>> math_thing(5)
      -0.11764705882352941
    """
    sum = 0
    product = 1
    
    for i in range(1, end + 1, 2):
        sum += i
    for i in range(2, end + 1, 2):
        product *= i
    
    avg = (sum + product) / 2
    return ((product - sum) / avg)

print(math_thing(5))

if __name__ == "__main__":
    import doctest
    doctest.testmod()
