def sum_even(end):
    sum = 0
    for i in range(2, end + 1, 2):
        sum += i
    return sum

print(sum_even(6))
