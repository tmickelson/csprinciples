def sum_odd(end):
    sum = 0
    for i in range(1, end + 1, 2):
        sum += i
    return sum

print(sum_odd(5))
