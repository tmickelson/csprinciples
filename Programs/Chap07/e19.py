def avg(start, finish):
    sum = 0
    product = 1
    for i in range(start, finish + 1):
        sum += i
    for i in range(start, finish + 1):
        product *= i
    return ((sum + product) / 2)

print(avg(1, 5))
