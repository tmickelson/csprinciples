numbers = [1, 2, 3, 4, 5]
def calc(num_list):
    product = 1  # Start out with 1
    for number in num_list:
        product = product * number
    return product
print(calc(numbers))

