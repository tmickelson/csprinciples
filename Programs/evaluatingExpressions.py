print("\nWelcome to Evaluating Expressions using Python")
print("Press ENTER to contunue")
input()

print("\nThere are three levels of operations in python.")
print()

print("In the first level, there is the negation opperator (-).")
print("The negation opperator flips the sign of number.")
print()

print("In the second level, there are four opperators: multiplication (*), division (/), floor division (//), and modulo (%).")
print("Floor division rounds the result down. Modulo takes the remainder of two numbers.")
print()

print("In the third level, there is addition and subtraction.")
print()

print("All expressions that are in parentheses are wvaluated before everything else.")
input()

print("\nExamples:")
print()

print("4 + -2 * 3 returns -2")
print("(4 + -2) * 3 returns 6\n")

print("Sometimes you can shorten expressions. For example if you wanted to calculate the cost of a trip you could do:")
print("""distance = 924.7
mpg = 35.5
gallons = distance / mpg
cost_per_gallon = 3.65
cost_trip = gallons * cost_per_gallon\n""")
print("Or you could do:")
print("""distance = 924.7
mpg = 35.5
cost_per_gallon = 3.65
cost_trip = distance / mpg * cost_per_gallon""")
