from turtle import *
space = Screen()
pen = Turtle()

def sqr(name, dst, ang, c1, c2, c3, c4):
    name.setheading(ang),
    name.color(c1)
    name.forward(dst)
    name.right(90)
    name.color(c2)
    name.forward(dst)
    name.right(90)
    name.color(c3)
    name.forward(dst)
    name.right(90)
    name.color(c4)
    name.forward(dst)
    name.right(90)

sqr(pen, 100, -45, "red", "yellow", "green", "blue")
