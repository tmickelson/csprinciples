from turtle import *
space = Screen()
t = Turtle()

def rect(name, w, h):
    name.forward(w)
    name.right(90)
    name.forward(h)
    name.right(90)
    name.forward(w)
    name.right(90)
    name.forward(h)
    name.right(90)

rect(t, 75, 150)
