from datetime import *
now = datetime.now()

new_m = 0
new_h = 0

def time_add(m, h):
    new_m = now.minute + m
    new_h = now.hour + h
    if new_m >= 60:
        new_m %= 60
        new_h += new_m // 60
    new_h %= 12
    if new_h == 0:
        new_h = 12
    return f"{new_h}:{new_m}"

print(time_add(70, 1))

