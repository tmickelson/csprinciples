from turtle import *
space = Screen()
pen = Turtle()

def tri(name, b, h):
    x = name.xcor()
    y = name.ycor()
    name.right(90)
    name.forward(h)
    name.left(90)
    name.forward(b)
    name.goto(x, y)
    
tri(pen, 75, 150)
