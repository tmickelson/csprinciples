def square(turtle, amt):
    turtle.forward(100)
    turtle.right(amt)
    turtle.forward(100)
    turtle.right(amt)
    turtle.forward(100)
    turtle.right(amt)
    turtle.forward(100)
    turtle.right(amt)

from turtle import *    # use the turtle library
space = Screen()        # create a turtle screen
malik = Turtle()        # create a turtle named malik
square(malik, 90)       # draw a square with malik
