def sum_func(start, stop):
    sum = 0
    for num in range(start, stop + 1):
        sum += num
    return sum

print(sum_func(1, 10))
