def sum_even_nums(end):
    counter = 0
    total = 0
    while counter < end + 1:
        total += counter
        counter += 2
    return(total)
    
print(sum_even_nums(10))
