from turtle import *        # use the turtle library
space = Screen()            # create a turtle screen (space)
zari = Turtle()             # create a turtle named zari
zari.setheading(90)         # Point due north
zari.forward(100)           # tell zari to move forward by 100 units
zari.right(120)             # turn right by 120 degrees
zari.forward(100)           # tell zari to move forward by 100 units
zari.right(120)             # turn right by 120 degrees
chad = Turtle()             # create a new turtle named chad
chad.color("orange")        # change the color chad draws with
chad.forward(100)           # tell chad to move forward by 100 units
chad.right(120)             # turn chad by 120 degrees
