from gasp import *
from time import sleep
import random

class Player():
    pass

class Robot:
    def __init__(self):
        self.x = random.randint(0, 63)
        self.y = random.randint(0, 47)
        self.shape = Box((self.x * 10 + 1, self.y * 10 + 1), 8, 8, filled=False)

def place_player():
    global player

    #player = Player()
    player.x = random.randint(0, 63)
    player.y = random.randint(0, 47)
    while collided(player, robots) == True:
        player.x = random.randint(0,63)
        player.y = random.randint(0,47)
    player.shape = Circle((10 * player.x + 5, 10 * player.y + 5), 5, filled=True)

def move_player():
    key = update_when("key_pressed")
    
    while key == 'f':
        remove_from_screen(player.shape)
        place_player()
        key = update_when('key_pressed')
    if key == 'e':
       player.x += -1
       player.y += 1
    if key == 'r':
       player.x += 0
       player.y += 1
    if key == 't':
        player.x += 1
        player.y += 1
    if key == 'd':
        player.x += -1
        player.y += 0
    if key == 'g':
        player.x += 1
        player.y += 0
    if key == 'c':
        player.x += -1
        player.y += -1
    if key == 'v':
        player.x += 0
        player.y += -1
    if key == 'b':
        player.x += 1
        player.y += -1
    if key == 'q':
        quit()
    
    if player.x < 0:
        player.x = 0
    if player.x > 63:
        player.x = 63
    if player.y < 0:
        player.y = 0
    if player.y > 47:
        player.y = 47

    move_to(player.shape, (10 * player.x + 5, 10 * player.y + 5))

def place_robots(level):
    global robots, dead_robots
    while len(robots) < 2 ** level:
        robot = Robot()
        robots.append(robot)

def move_robots():
    global robots, player
    for robot in robots:
        if player.x > robot.x:
            robot.x += 1
        elif player.x < robot.x:
            robot.x -= 1
        if player.y > robot.y:
            robot.y += 1
        elif player.y < robot.y:
            robot.y -= 1
        move_to(robot.shape, (robot.x * 10 + 1, robot.y * 10 + 1))

def collided(player, robots):
    for robot in robots:
        if player.x == robot.x and player.y == robot.y:
            return True
    return False

def check_collisions():
    global finished, robots, dead_robots, living_robots
    living_robots = []
    dead_robots = []
    for robot in robots:
        if collided(robot, dead_robots):
            continue
        if collided(player, robots + dead_robots):
            done = True
            text_end = Text("You’ve been caught!", (320, 240), size = 50)
            sleep(1)
            remove_from_screen(text_end)
            finished = True
            return
        dead_robot = robot_crashed(robot)
        if not dead_robot:
            living_robots.append(robot)
        else:
            remove_from_screen(dead_robot.shape)
            dead_robot.shape = Box((robot.x * 10 + 1, robot.y * 10 + 1), 8, 8, filled=True)
            dead_robots.append(dead_robot)
    robots = []
    for living_robot in living_robots:
        if not collided(living_robot, dead_robots):
            robots.append(living_robot)
    if not robots:
        text_end = Text("You win!", (320, 240), size = 50)
        sleep(1)
        remove_from_screen(text_end)
        finished = True
        return

def robot_crashed(bot):
    for robot in robots:
        if robot == bot:
            return False
        if robot.x == bot.x and robot.y == bot.y:
            return robot
    return False

def main():
    global player, finished, another, robots, dead_robots, living_robots
    level = int(input("What level (1-10): "))
    
    begin_graphics()
    finished = False
    player = Player()
    robots = []
    
    while True:
        place_robots(level)
        place_player()
        while not finished:
            move_player()
    
            move_robots()
            check_collisions()
        
        text_again = Text("Play again? (Y/n)", (320, 240), size = 50)
        key = update_when("key_pressed")
        print(f"key pressed: {key}")
        if key == 'y':
            level += 1
            finished = False
            remove_from_screen(text_again)
            for i in dead_robots:
                remove_from_screen(i.shape) 
            dead_robots = []
            for j in robots:
                remove_from_screen(j.shape)
            robots = []
            if not living_robots:
                for k in living_robots:
                    remove_from_screen(k.shape)
                living_robots = []
            remove_from_screen(player.shape)
        else:
            quit()
    end_graphics()


if __name__ == "__main__":
    main()

