s1 = "hi"
s2 = "My namesake is Bob, and he and I love to eat ham."
p1 = s1.capitalize()
p2 = s2[0:2].lower()
p3 = s2[3:7]
p4 = s2[12:14]
p5 = s2[15:18]
p6 = s2[20:23]
p7 = s2[31:32]
p8 = s2[20:21]
p9 = s2[5:6]
p10 = len(s1)
print(f"{p1} {p2} {p3} {p4} {p5} {p6} {p7} {p8}{p9} {p10}")
