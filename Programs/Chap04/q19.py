amnt_per_week = 20
total = 200
num_weeks = total / amnt_per_week
num_months = num_weeks / 4
print(f"It will take {num_months} months to earn {total} if you make {amnt_per_week} dollars a week.")
