tank_capacity = 10
num_gallons = tank_capacity / 4
miles_per_gallon = 32
num_miles = miles_per_gallon * num_gallons
print(f"You can go {num_miles} miles.")
