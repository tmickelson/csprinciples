print(input("What is your name? ")[0].lower())

# Or you could do this:
# name = input("What is your name? ")
# letter = name[0]
# letter = letter.lower()
# print(letter)
