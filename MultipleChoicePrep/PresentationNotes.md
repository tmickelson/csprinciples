# Presentation 1
> Noah, Anupama, Eimi, Rockwell (3/13/23)

- if you dont know a problem go back to it later
- study
- use practice tests
- resources:
  - khan academy
  - collage board: has some past questions
  - apcsp test guide
- repetition

(links in noahs repo)

# Jeff is gone
> [Source for questions](http://apcspths.pbworks.com/w/file/fetch/121964706/Sample%2520Exam%2520Questions.pdf)

## Question 1
> Question 1

A video-streaming Web site uses 32-bit integers to count the number of times each video has been played. In anticipation of some videos being played more times than can be represented with 32 bits, the Web site is planning to change to 64-bit integers for the counter. 
Which of the following best describes the result of using 64-bit integers instead of 32-bit integers? 
- [ ] 2 times as many values can be represented.
- [ ] 32 times as many values can be represented.
- [x] 2^32 times as many values can be represented.
- [ ] 32^2 times as many values can be represented.

The correct answer is 3 because each bit added doubles the maximum size of the int. 

## Question 2
> Question 2

A programmer completes the user manual for a video game she has developed and realizes she has reversed the roles of goats and sheep throughout the text. Consider the programmer’s goal of changing all occurrences of “goats” to “sheep” and all occurrences of “sheep” to “goats.” The programmer will use the fact that the word “foxes” does not appear anywhere in the original text.
Which of the following algorithms can be used to accomplish the programmer’s goal? 
- [ ] First, change all occurrences of “goats” to “sheep.” Then, change all occurrences of “sheep” to “goats.”
- [ ] First, change all occurrences of “goats” to “sheep.” Then, change all occurrences of “sheep” to “goats.” Last, change all occurrences of “foxes” to “sheep.”
- [x] First, change all occurrences of “goats” to “foxes.” Then, change all occurrences of “sheep” to “goats.” Last, change all occurrences of “foxes” to “sheep.”
- [ ] First, change all occurrences of “goats” to “foxes.” Then, change all occurrences of “foxes” to “sheep.” Last, change all occurrences of “sheep” to “goats.”

The correct answer is 3 because the first two directly change goats to sheep and 4 does the same thing with an extra step. 

## Question 3
> Question 4

The figure below shows a circuit composed of two logic gates. The output of the circuit is true.

```
     [FALSE]  [A]
           \  /
 [TRUE]    [OR]
      \   /
      [AND]
        |
     [TRUE]
```

- [x] Input `A` must be true.
- [ ] Input `A` must be false.
- [ ] Input `A` can be either `true` or `false`.
- [ ] There is no possible value of input `A` that will cause the circuit to have the output `true`.

The correct answer is 1 because `and` needs both inputs to be true, and `or` needs only one input to be true. 

