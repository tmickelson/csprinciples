# Zulip Terminal Installation

## Installation

Update pip using `python3 -m pip install --upgrade pip`

Install zulip term using `pip install zulip-term`

After you install zulip-term you need to add `export PATH="/Users/1008814/Library/Python/3.8/bin:$PATH"` to the .zshrc file in you home directory

Run `zulip-term`

If you have not signed into the Zulip Terminal application before, you will have to enter the zulip chat url \(nvcc.zulipchat.com\). 
Enter the email that your Zulip account is associated with. 
Enter your password. MacOS automatically hides your password behind a key icon. 

Open Terminal Preferences \(Cmd + ,\) \> Profiles \> Keyboard \> check Use Option as Meta key. 


## Desktop Shortcut

Go to desktop using `cd ~/Desktop` 

Create a file called Zulip Terminal using `touch Zulip\ Terminal.command` 

Modify the permision of Zulip Terminal so that you can run it using `chmod u+x Zulip\ Terminal.command` 

On your desktop, right click (tap with two fingers) on Zulip Terminal.command \> Get Info > Name \& Extention \> check Hide extention

Right click on Zulip Terminal again \> Open With \> TextEdit. In TextEdit put `zulip-term` 


## Other Stuff

[Tutorial](https://github.com/zulip/zulip-terminal/blob/main/docs/getting-started.md) 

[FAQ](https://github.com/zulip/zulip-terminal/blob/main/docs/FAQ.md) 

Help Menu - ? (`Shift` + `/`) 

| Option | Result |
| ------ | ------ |
| --theme, -t | Changes the theme for current session \(theme follows\) |
| zt\_dark | Dark theme \(Default\) |
| zt\_light | Light theme |
| zt\_blue | Blue theme |
| gruvbox\_dark | Alt. dark theme |
| gruvbox\_light | Alt. light theme |
| --autohide | Hides Streams \& Topics and Users when not in use |

To have all your session use these settings put the options in your Zulip Terminal.command file

Thanks Aaron for helping me figure out how to install the latest version.
