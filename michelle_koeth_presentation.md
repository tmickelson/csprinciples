# Michelle Koeth Presentation

## notes
- jeff is a bird watcher
- edge computing
  - less central processiing
  - more privacy
  - less network traffic
  - specilized hardware
  - easier redundancy
- edge computing board
  - raspi
  - arduino
  - esp32
- tinyml
  - low power/always on
  - data collected in the feild
- types
  - digital signal processing (not ml)
  - digital neural network (ml)
  - supervised learning
- dev cycle
  1. idea
  2. research
  3. data colectiom
  4. architecting and training
  5. deployment
  6. testing
  7. goto 3

## deffinitions
- Edge computing
  - Computing data at the source
- tensorflowlite
  - ml lib
- convolution
  - filter that reduces amount of data
- quantizatation
  - converting analog to digital
- pruning
  - removing redundant or unnessicary synapses or neurons
- transfer learning
  - preprogramed base template
- supervised learnig
  - training with labeled data

## comments
- 

## questions
- 


