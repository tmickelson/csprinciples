# Quiz Reflections

## Quiz 1

### Question 4

> An automobile company uses GPS technology to implement a smartphone app for its customers that they can use to call their self-driving electric cars to their location. When the app is used, the coordinates of the car owner are sent to their car. The car then proceeds to drive itself from the parking lot to its owner. 
>
> What is a possible concern for user privacy when using this app?
>
> a. Idling periods for which the car is running in anticipation of the signal would cause excess fuel consumption  
> b. Unauthorized access to vehicles through the app  
> c. User location data could be used by companies and third-party stakeholders  
> d. Interference of signals if two people use the app simultaneously and close by  

I chose option b because I misread/misinterpreted privacy to mean security. 

The correct option is c because that is the only option that involves privacy. Also Big Tech likes to sell data. 

### Question 5

> A programmer has developed an algorithm that computes the sum of integers taken from a list. However, the programmer wants to modify the code to sum up even numbers from the list. What programming structure can the programmer add to do this?
>
> a. Sequencing  
> b. Iteration  
> c. Searching  
> d. Selection  

I chose option b because I missed the add part. (I don't really remember why I chose this option)

The correct option is d because a number can only be added if it is even, therefore even numbers must be selected. 

### Question 7 

> Every night at 12:00 am, you notice that some files are being uploaded from your PC device to an unfamiliar website without your knowledge. You check the file and find that these files are from the day's work at the office. What is this an example of?
>
> a. Phishing attack  
> b. Keylogger attack  
> c. Computer virus  
> d. Malware insertion point  

I chose option d because I misread/misinterpreted "uploaded from your PC" as "uploaded to your PC". 

The correct option is c because a virus is the only thing on the list that runs on that runs on a computer and uploads files. (A keylogger might upload the keystroke file, but that is mainly it.)

## Quiz 2

### Question 1

> Chicago recently hired a firm to install security cameras with facial recognition software around Central Park. However, a brawling incident at the park led to the arrest of an innocent person based on the recognized facial features of one of the belligerents.
>
> What should the firm do to remove the bias?
>
> a. Improve the training materials used for training and interpreting by the software  
> b. Use photos from a more diverse community for software training  
> c. Ask lawmakers to create stronger regulations for how facial recognition software is used  
> d. Add new team members from diverse communities to help stop biased operations of the software  

I chose option d because all options but c looked correct. (Now rereading them, I see the inconsistencies between the answers)

The correct option is b because a more diverse community will most likely have less bias

### Question 9

> Using a binary search, how many iterations would it take to find the letter w?
> &nbsp;&nbsp;&nbsp;&nbsp;`str <- [a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z]`
>
> a. 2  
> b. 3  
> c. 23  
> d. 24  

I chose option c because I did not know what a binary sort was. 

The correct option is b because a binary sort would split it in half, go in the top half (because the sort is behind w), split that in half, go into the top half (because the sort is behind w), on and on until it hits the target. (Works only with sorted datasets)

### Question 10

> Using a linear search, how many iterations would it take to find the letter x?
> &nbsp;&nbsp;&nbsp;&nbsp;`str <- [a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z]`
>
> a. 2  
> b. 3  
> c. 23  
> d. 24  

I chose option c because I did not know what a linear sort was (and I didn't realize the letter changed). 

The correct option is d because a linear sort goes through each item and checks if it the correct one. Is index 1 the item? no; Is index 2 the item? no; Is index 3 the item? no; on and on until it hits the target. (Works for both randomized and sorted datasets)

## Quiz 3

### Question 6

> The same task is performed through sequential and parallel computing models. The time taken by the sequential setup was 30 seconds, while it took the parallel model only 10 seconds.
>
> What will be the speedup parameter for working with the parallel model from now on?
>
> a. 0.333  
> b. 3  
> c. 300  
> d. 20  

I chose option b because speed up means get faster, and 30/10 is 3. 

The correct option is a because 30 * 0.333 is 10.

## Quiz 4

### Question 8

> Which of the following best describes the behavior of the procedure? Select two answers.
>
> ```
> PROCEDURE mystery (list, target)
>   count <- 0
>   FOR EACH n IN list
>     IF (n = target)
>       count <- count + 1
>     ELSE
>       count <- 0
> DISPLAY (count)
> ```
>
> a. The program correctly displays the count if the target is not in the list.  
> b. The program never correctly displays the correct value for count.  
> c. The program correctly displays the count if the target appears once in the list and also at the end of the list.  
> d. The program always correctly displays the correct value for count.  

I chose option d (just option d) because I did not see the select two or the fact that count is reset to zero if it is not correct. 

The correct option is a and c because it resets the value for count when target is not the current element. 

### Question 10

> If your algorithm needs to search through a list of unsorted words, what type of search would you use?
>
> a. Linear search  
> b. Binary search  
> c. Bubble sort  
> d. Insertion sort  

I chose option b because I did not know what either sort was. 

The correct option is a because unsorted lists can only be searched with a linear search

